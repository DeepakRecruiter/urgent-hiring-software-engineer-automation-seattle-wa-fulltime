We have a fulltime Opportunity with one of our Direct Client in Seattle WA

Title : Software Engineer, Automation

Job Summary:
We are looking for innovators that want to join a startup team on a journey to transform enterprise storage. Come work on a modern technology stack including CoreOS, Docker, Kubernetes and Golang. You must have a passion to deliver great products, create great user experiences, and work in a fast paced, customer-driven, product organization.
Job Responsibilities:
-  Help improve the quality, performance, and resiliency of our product.
-  Design, build, and manage advanced automated testing frameworks.
-  Develop comprehensive integration and performance test suites.
-  Work with fellow engineers to deliver enterprise-class products.
-  Participate in daily stand-ups and work in an agile environment within a startup culture.
Who you are:
You’re an engineer who can turn ideas into efficient, reliable and maintainable code. You will be successful in this role if you:
-  Enjoy helping others around you grow as developers and be successful.
-  Can be autonomous and self-driven.
-  Have an entrepreneurial mindset.
-  Have excellent written and verbal communication skills.
-  Get inspired on a daily basis, think of new ideas and like to share them with others.
-  Love open source software development and being part of an community.
Job Qualifications:
-  Fluency in languages like Python or Go.
-  Experience in DevOps process and CI/CD pipeline tools like Jenkins, Bamboo, etc.
-  Demonstrable troubleshooting and debugging ability.
-  Experience with low-level performance testing.
-  Experience with Linux distributions.
-  Experience in building maintainable and well tested code.
-  A four year BS/BA degree or equivalent in Computer Science or related technical field.


Regards,

Deepak Sharma
Sr. Recruiter
APAC Solution
+1 206-905-1241
www.apacsolution.com
